try:
    from django.conf.urls import patterns, url
except ImportError:
    from django.conf.urls.defaults import patterns, url

from . import views

urlpatterns = patterns('',
    url(r'^offer/create/$',              views.OfferCreateView.as_view(),   name='offer_create'),
    url(r'^demand/create/$',             views.DemandCreateView.as_view(),  name='demand_create'),
    url(r'^detail/(?P<pk>\d+)/$',        views.OfferDetailView.as_view(),   name='detail'),
    url(r'^offer/update/(?P<pk>\d+)/$',  views.OfferUpdateView.as_view(),   name='offer_update'),
    url(r'^demand/update/(?P<pk>\d+)/$', views.DemandUpdateView.as_view(),  name='demand_update'),
    url(r'^delete/(?P<pk>\d+)/$',        views.OfferDeleteView.as_view(),   name='delete'),
    url(r'^renew/(?P<pk>\d+)/$',         views.OfferRenewView.as_view(),    name='renew'),

    url(r'^$',                           views.OfferListView.as_view(),),
    url(r'^offer/$',                     views.OfferListView.as_view(),     name='offer_list'),
    url(r'^demand/$',                    views.DemandListView.as_view(),    name='demand_list'),
)

