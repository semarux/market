﻿
function toggle_notes(id, detail, hide) {
  text = $('#dbutton'+id+' span').html();
  if (text == hide) {
    $('#line'+id).hide(); 
    $('#notes'+id).hide();
    $('#dbutton'+id+' span').html(detail);
  } else {
    $('#line'+id).show(); 
    $('#notes'+id).show();
    $('#dbutton'+id+' span').html(hide);
  }
}

function delete_offer_ajax(url, token, redirect) {
    $.post(
      url,
      {
        "csrfmiddlewaretoken": token,
      },
      function (data) {
        if (data["id"] != 0) {
          obj = $('#offer_short'+data["id"])
          if (obj) { obj.remove(); }
          if (redirect) { window.location=redirect; }
        }
      },
      "json"
    );
};

function filter_by_price(price, pk) {
    $('#id_price_min').val(price);
    $('#id_price_max').val(price);
    $('#id_goods').val(pk);
    $('#search_form').submit();
}

function add_country(query) {
    id_country = $('#id_country').val();
    query.country = id_country;
    if (id_country == 3159) {
        query.region = $('#id_region').val();
    };
    $('#id_location_1').val('');
}
