# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Offer.caliber_max'
        db.alter_column('market_offer', 'caliber_max', self.gf('django.db.models.fields.FloatField')(null=True))

        # Changing field 'Offer.caliber_min'
        db.alter_column('market_offer', 'caliber_min', self.gf('django.db.models.fields.FloatField')(null=True))

    def backwards(self, orm):

        # Changing field 'Offer.caliber_max'
        db.alter_column('market_offer', 'caliber_max', self.gf('django.db.models.fields.FloatField')(default=None))

        # Changing field 'Offer.caliber_min'
        db.alter_column('market_offer', 'caliber_min', self.gf('django.db.models.fields.FloatField')(default=None))

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '80', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'deal.businesstype': {
            'Meta': {'ordering': "['title']", 'object_name': 'BusinessType'},
            'company_function': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'business_types'", 'to': "orm['deal.CompanyFunction']"}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page_address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'deal.city': {
            'Meta': {'ordering': "['title']", 'object_name': 'City'},
            'country_code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'flag': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '10'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            u'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lng': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '10'}),
            'page_address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['deal.City']"}),
            u'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'titlefull': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'titlefull_en': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'titlefull_ru': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'deal.company': {
            'Meta': {'ordering': "['title']", 'object_name': 'Company'},
            'banner': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'banner'", 'sizes': '((500, 200), (400, 170))', 'max_length': '100', 'blank': 'True', 'null': 'True'}),
            'business_type': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['deal.BusinessType']", 'symmetrical': 'False'}),
            'company_function': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['deal.CompanyFunction']"}),
            'company_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['deal.CompanyType']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['deal.Country']"}),
            'date_create': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'date_state_registration': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inn': ('deal.helpers.formatChecker.ContentTypeRestrictedFileField', [], {'content_types': "['image/jpg', 'image/jpeg', 'image/bmp', 'image/png', 'image/gif', 'image/tiff', 'image/psd', 'image/cdr', 'image/ai', 'application/pdf', 'application/zip', 'application/x-rar-compressed', 'application/x-7z-compressed', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/plain', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/odt', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.sun.xml.calc', 'application/vnd.sun.xml.calc.template', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.ms-powerpoint', 'application/xml', 'text/html', 'text/csv']", 'max_upload_size': '1048576', 'null': 'True', 'max_length': '100', 'blank': 'True'}),
            'inn_checked': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '1'}),
            'inn_num': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'legal_address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'legal_address_location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'legal_address_location'", 'to': "orm['deal.City']"}),
            'legal_address_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'legal_address_region'", 'null': 'True', 'to': "orm['deal.City']"}),
            'logo': ('django_thumbs.db.models.ImageWithThumbsField', [], {'name': "'logo'", 'sizes': '((130, 130), (138, 170))', 'max_length': '100', 'blank': 'True', 'null': 'True'}),
            'meta_description': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '1000', 'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'ogrn': ('deal.helpers.formatChecker.ContentTypeRestrictedFileField', [], {'content_types': "['image/jpg', 'image/jpeg', 'image/bmp', 'image/png', 'image/gif', 'image/tiff', 'image/psd', 'image/cdr', 'image/ai', 'application/pdf', 'application/zip', 'application/x-rar-compressed', 'application/x-7z-compressed', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'text/plain', 'application/vnd.ms-excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/odt', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.sun.xml.calc', 'application/vnd.sun.xml.calc.template', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.ms-powerpoint', 'application/xml', 'text/html', 'text/csv']", 'max_upload_size': '1048576', 'null': 'True', 'max_length': '100', 'blank': 'True'}),
            'ogrn_checked': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '1'}),
            'ogrn_num': ('django.db.models.fields.CharField', [], {'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['deal.Company']", 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'subscription': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'super_company': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'max_length': '1'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True'})
        },
        'deal.companyfunction': {
            'Meta': {'ordering': "['title']", 'object_name': 'CompanyFunction'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'page_address': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'deal.companytype': {
            'Meta': {'ordering': "['title']", 'object_name': 'CompanyType'},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'description_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'description_ru': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'label_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'label_ru': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'deal.country': {
            'Meta': {'ordering': "(u'name',)", 'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'deal.moneytype': {
            'Meta': {'ordering': "['title']", 'object_name': 'MoneyType'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'title_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'title_ru': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'market.goods': {
            'Meta': {'ordering': "['title']", 'object_name': 'Goods'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'market.offer': {
            'Meta': {'ordering': "['date_create']", 'object_name': 'Offer'},
            'caliber_max': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'caliber_min': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'company': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['deal.Company']"}),
            'date_create': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'from_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['deal.Country']"}),
            'from_location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['deal.City']"}),
            'from_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['deal.City']"}),
            'goods': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['market.Goods']"}),
            'grade': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'manufacturer': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'money_type_wd': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['deal.MoneyType']"}),
            'money_type_wod': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['deal.MoneyType']"}),
            'notes': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'offer_type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'origin_country': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'+'", 'to': "orm['deal.Country']"}),
            'pack': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'payment_type': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['market.Payment']"}),
            'price_wd': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'price_wod': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'to_country': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['deal.Country']"}),
            'to_location': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['deal.City']"}),
            'to_region': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': "orm['deal.City']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'market.payment': {
            'Meta': {'ordering': "['title']", 'object_name': 'Payment'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'market.photo': {
            'Meta': {'object_name': 'Photo'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'img': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'offer': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['market.Offer']"})
        }
    }

    complete_apps = ['market']