﻿# coding: utf-8

from django import forms
from django.forms.models import inlineformset_factory
from django.utils.translation import ugettext_lazy as _

from selectable.forms.widgets import AutoComboboxSelectWidget

from deal.models.cargos import City, MoneyType
from deal.helpers.lookups import CityLookup

from .models import Offer, Photo, Goods, Country, MoneyType, Payment

class PhotoForm(forms.ModelForm):
    class Meta:
        model = Photo
        
    def __init__(self, *args, **kwargs):
        super(PhotoForm, self).__init__(*args, **kwargs)

        self.fields['img'].label = ''
        self.fields['img'].widget = forms.FileInput()

OfferPhotoInline = inlineformset_factory(Offer, Photo, fields=('img',), can_delete=False, extra=4, max_num=4, form=PhotoForm)

class BaseForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)

        self.fields['goods'].empty_label = _(u'Выберите тип товара..')
        self.fields['origin_country'].empty_label = _(u'Выберите страну происхождения..')
        self.fields['country'].empty_label = _(u'Выберите страну..')
        self.fields['region'].empty_label = _(u'Выберите регион..')
        self.fields['region'].queryset = City.objects.filter(flag=2, country_code=u'ru').all()
        try: self.initial['money_type'] = MoneyType.objects.get(title='RUB').id
        except: pass
        self.fields['money_type'].empty_label = None

    def clean(self):
        cleaned_data = super(BaseForm, self).clean()

        country = self.cleaned_data.get('country')
        region = self.cleaned_data.get('region')
        if country and country.id == 3159 and not region:
            self._errors['region'] = self.error_class(forms.ValidationError(self.fields['region'].error_messages['required']).messages)

        return cleaned_data

    class Meta:
        model = Offer
        widgets = {   
            'offer_type': forms.RadioSelect(attrs={'id':'offer_type', 'class':'offer_type'}),
            'goods': forms.Select(attrs={'class':'select small long'}),
            'grade': forms.TextInput(attrs={'class':'text small long', 'placeholder':_(u'Укажите сорт..')}),
            'caliber_min': forms.TextInput(attrs={'class': 'small select middle text fr', 'placeholder':_(u'От..')}),
            'caliber_max': forms.TextInput(attrs={'class': 'small select middle text fr', 'placeholder':_(u'До..')}),
            'manufacturer': forms.TextInput(attrs={'class':'text small long', 'placeholder':_(u'Укажите производителя..')}),
            'origin_country': forms.Select(attrs={'class': 'select small long'}),
            'pack': forms.TextInput( attrs={'class':'small text long', 'placeholder':_(u'Укажите вид упаковки..')}),
            'country': forms.Select(attrs={'class': 'select small long'}),
            'region': forms.Select(attrs={'class': 'select small long'}),
            'location': AutoComboboxSelectWidget(lookup_class=CityLookup, attrs={'class':'text small long', 'placeholder': _(u'Введите город..')}),
            'delivery': forms.CheckboxInput(),
            'price': forms.widgets.TextInput(attrs={'class':'text small middle fr', 'placeholder': _(u'Укажите стоимость..')}),
            'money_type': forms.widgets.Select(attrs={'class':'small select short fr'}),
            'payment_type': forms.CheckboxSelectMultiple(),
            'notes': forms.widgets.Textarea(attrs={'class':'textarea small long', 'rows':8, 'placeholder':_(u'Укажите дополнительные сведения при необходимости')}),
            'user': forms.widgets.HiddenInput(),
            'company': forms.widgets.HiddenInput(),
        }

class OfferCreateForm(BaseForm):
    class Meta(BaseForm.Meta):
        fields = (
            'offer_type',
            'goods','grade','caliber_min','caliber_max','manufacturer','origin_country',
            'pack','country','region','location',
            'price','money_type','payment_type',
            'notes',
        )

    def __init__(self, *args, **kwargs):
        super(OfferCreateForm, self).__init__(*args, **kwargs)

        self.initial['offer_type'] = 1


class DemandCreateForm(BaseForm):
    class Meta(BaseForm.Meta):
        fields = (
            'offer_type',
            'goods','grade','caliber_min','caliber_max','origin_country',
            'pack','country','region','location','delivery',
            'price','money_type','payment_type',
            'notes',
        )

    def __init__(self, *args, **kwargs):
        super(DemandCreateForm, self).__init__(*args, **kwargs)

        self.initial['offer_type'] = 0

        self.fields['notes'].label = _(u'Примечание')
        self.fields['country'].label = _(u'Мое местонахождение')

class BaseSearchForm(forms.Form):
    # Нужно, чтобы определить, заполнялась ли форма, или дергались переключатели вне ее
    #  страницы в пагинаторе или фильтр "мои/партнеров и т.д."
    # Все данные текстовые, т.к. сверяем с еще не почищеными данными из GET
    initial = {
        'goods': '',
        'caliber_min': '',
        'caliber_max': '',
        'origin_country': '',
        'price_min': '',
        'price_max': '',
        'money_type': '1',
        'payment_type': '',
        'country': '',
        'region': '',
        'location': '',
        'delivery': '2',
    }

    offer_view_filter = forms.CharField(widget=forms.HiddenInput(), required=False)
    offer_type = forms.CharField(widget=forms.HiddenInput(), required=False)
    goods = forms.ModelChoiceField(widget=forms.Select(attrs={'class':'select small long'}), label=Offer.goods.field.verbose_name, empty_label=_(u'Любое'), queryset=Goods.objects.all(), required=False)
    caliber_min = forms.CharField(label=Offer._meta.get_field('caliber_min').verbose_name, widget=forms.TextInput(attrs={'class': 'small select middle text fr', 'placeholder':_(u'От..')}), required=False)
    caliber_max = forms.CharField(label=Offer._meta.get_field('caliber_max').verbose_name, widget=forms.TextInput(attrs={'class': 'small select middle text fr', 'placeholder':_(u'До..')}), required=False)
    origin_country = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'select small long'}), label=Offer.origin_country.field.verbose_name, empty_label=_(u'Любая'), queryset=Country.objects.all(), required=False)
    price_min = forms.DecimalField(label=_(u'Цена, за кг'), widget=forms.TextInput(attrs={'class': 'small short text fr', 'placeholder':_(u'От..')}), required=False)
    price_max = forms.DecimalField(widget=forms.TextInput(attrs={'class': 'small short text fr', 'placeholder':_(u'До..')}), required=False)
    money_type = forms.ModelChoiceField(widget=forms.Select(attrs={'class':'small select short short2 fr'}), empty_label=_(u'Любая'), label=Offer.money_type.field.verbose_name, queryset=MoneyType.objects.all(), required=False)
    payment_type = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), label=Offer.payment_type.field.verbose_name, queryset=Payment.objects.all(), required=False)
    country = forms.ModelChoiceField(widget=forms.Select(attrs={'class':'select small long'}), label=Offer.country.field.verbose_name, empty_label=_(u'Любая страна'), queryset=Country.objects.all(), required=False)
    region  = forms.ModelChoiceField(widget=forms.Select(attrs={'class':'select small long'}), label=Offer.region.field.verbose_name, empty_label=_(u'Любой регион'), queryset = City.objects.filter(flag=2, country_code=u'ru').all(), required=False)
    location = forms.ModelChoiceField(widget=AutoComboboxSelectWidget(lookup_class=CityLookup, attrs={'class':'text small long', 'placeholder': _(u'Любой город')}), queryset=City.objects.all(), required=False)
    delivery = forms.NullBooleanField(required=False, widget=forms.widgets.RadioSelect(choices=[(1, _(u'Да')), (0, _(u'Нет')), (2, _(u'Неважно'))]), initial=2)

    def __init__(self, *args, **kwargs):
        super(BaseSearchForm, self).__init__(*args, **kwargs)

        try: self.initial['money_type'] = MoneyType.objects.get(title='RUB').id 
        except: pass
        self.fields['delivery'].label = _(u'Нужна доставка')

class OfferSearchForm(BaseSearchForm):
    def __init__(self, *args, **kwargs):
        super(OfferSearchForm, self).__init__(*args, **kwargs)

        self.initial['offer_type'] = 1

class DemandSearchForm(BaseSearchForm):

    def __init__(self, *args, **kwargs):
        super(DemandSearchForm, self).__init__(*args, **kwargs)

        self.initial['offer_type'] = 0


