# coding: utf-8

import os

from django.db import models
from django.db.models import Q, FileField
from django.db.models.signals import post_delete
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.conf import settings
from django.core.files.storage import default_storage

from deal.models.cargos import City, PackType, MoneyType
from deal.models.members import Country, Company

class Payment(models.Model):
    title = models.CharField(max_length=200, verbose_name=_(u'Вариант оплаты'))

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _(u'Вариант оплаты')
        verbose_name_plural = _(u'Варианты оплаты')
        ordering = ['title',]

class Goods(models.Model):
    title = models.CharField(max_length=200, verbose_name=_(u'Товар'))

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _(u'Товар')
        verbose_name_plural = _(u'Товары')
        ordering = ['title',]

OFFER_TYPE_CHOICES = [
    (1, _(u'Предложение')),
    (0, _(u'Спрос')),
]

class Offer(models.Model):
    offer_type  = models.IntegerField(default=1, verbose_name=u'Тип', choices=OFFER_TYPE_CHOICES)
    goods = models.ForeignKey(Goods, verbose_name=u'Наименование')
    grade = models.CharField(blank=True, max_length=100, verbose_name=_(u'Сорт'), help_text=_(u'Укажите сорт..'))
    caliber_min = models.FloatField(blank=True, null=True, verbose_name=_(u'Калибр'), help_text=_(u'От..'))            
    caliber_max = models.FloatField(blank=True, null=True, verbose_name=_(u'Калибр'), help_text=_(u'До..'))            
    manufacturer = models.CharField(blank=True, null=True, max_length=100, verbose_name=_(u'Производитель'), help_text=_(u'Укажите производителя..')) 
    origin_country = models.ForeignKey(Country, related_name='+', verbose_name=_(u'Страна происхождения'), help_text=_(u'Выберите страну происхождения..'))

    pack  = models.CharField(max_length=100, blank=True, verbose_name=_(u'Вид упаковки'), help_text=_(u'Введите вид упаковки..'))
    # География
    country = models.ForeignKey(Country, related_name='+', verbose_name=_(u'Местонахождение'), help_text=_(u'Страна'))
    region = models.ForeignKey(City, related_name='+', blank=True, null=True, verbose_name=_(u'Регион'), help_text=_(u'Регион'))
    location = models.ForeignKey(City, related_name='+', verbose_name=_(u'Город/Регион'), help_text=_(u'Город/Регион'))
    delivery = models.BooleanField(verbose_name=_(u'Нужна доставка'), help_text=_(u'Нужна доставка'), default=False)

    price = models.DecimalField(max_digits=15, decimal_places=2, verbose_name=_(u'Цена, за кг'), help_text=_(u'Укажите стоимость..'))
    money_type = models.ForeignKey(MoneyType, related_name='+', verbose_name=_(u'Валюта'))
    payment_type = models.ManyToManyField(Payment, related_name='+', blank=True, null=True, verbose_name=_(u'Вариант оплаты'), help_text=_(u'Выберите вариант оплаты..'))

    notes = models.CharField(max_length=200, blank=True, null=True, verbose_name=_(u'Описание'), help_text=_(u'Укажите дополнительные сведения при необходимости'))

    user  = models.ForeignKey(User, verbose_name=_(u'Автор'))
    company = models.ForeignKey(Company, verbose_name=_(u'Компания'))
    date_create = models.DateTimeField(verbose_name=_(u'Дата создания предложения'), auto_now_add=True)

    def image_url(self):
        photo = Photo.objects.filter(offer=self.id)
        if photo:
            url = photo[0].img.url
        else:
            url = settings.STATIC_URL+"images/nophoto.png"
        return url
    image_url.allow_tags = True
    image_url.short_description = _(u"Изображение")

    def __unicode__(self):
        return u'%s %s' % (self.id, self.goods)

    def payments(self):
        return u', '.join(map(unicode, self.payment_type.select_related()))

    class Meta:
        verbose_name = _(u'Предложение')
        verbose_name_plural = _(u'Предложения')
        ordering = ['date_create',]


def file_cleanup(sender, **kwargs):
    for fieldname in sender._meta.get_all_field_names():
        try:
            field = sender._meta.get_field(fieldname)
        except:
            field = None
        if field and isinstance(field, FileField):
            inst = kwargs['instance']
            f = getattr(inst, fieldname)
            m = inst.__class__._default_manager
            if hasattr(f, 'path') and os.path.exists(f.path) and not m.filter(**{'%s__exact' % fieldname: getattr(inst, fieldname)}).exclude(pk=inst._get_pk_val()):
                try:
                    default_storage.delete(f.path)
                except:
                    pass

class Photo(models.Model):
    offer = models.ForeignKey(Offer)
    img = models.ImageField(upload_to='market', verbose_name=_(u'Фотография'))

    class Meta:
       verbose_name = _(u'Фотография')
       verbose_name_plural = _(u'Фотографии')

post_delete.connect(file_cleanup, sender=Photo)