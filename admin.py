# coding: utf-8

from django.contrib import admin

from .models import Payment, Goods, Offer, Photo

admin.site.register(Payment)
admin.site.register(Goods)
admin.site.register(Offer)
admin.site.register(Photo)
