# coding: utf-8

import json
import datetime

from django.http import HttpResponse, HttpResponseForbidden, HttpResponsePermanentRedirect
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse
from django.views.generic import CreateView, DetailView, UpdateView, ListView, DeleteView, View
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.edit import FormView
from django.db.models import Q, Min, Max, Avg
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from .models import Offer, Photo
from .forms import OfferCreateForm, DemandCreateForm, OfferPhotoInline, OfferSearchForm, DemandSearchForm
from deal.models.partnerships import Partnership

class OfferFormMixin(object):
    model = Offer

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.company = self.request.user.employee.company
        obj.save()

        photo_inline_form = OfferPhotoInline(self.request.POST, self.request.FILES, instance=obj)
        if photo_inline_form.is_valid():
            photo_inline_form.save()

        form.save_m2m()
        return super(OfferFormMixin, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(OfferFormMixin, self).get_context_data(**kwargs)
        self.photo_inline = OfferPhotoInline
        if self.object:
          self.photo_inline.can_delete = True
        else:
          self.photo_inline.can_delete = False
        context['photo_inline'] = self.photo_inline(instance=self.object)
        return context

    def get_success_url(self):
        return reverse('market:detail', kwargs={'pk': self.object.pk})

class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(*args, **kwargs)

class AuthorRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        result = super(AuthorRequiredMixin, self).dispatch(request, *args, **kwargs)
        if self.get_object().user != self.request.user:
            return HttpResponseForbidden()
        return result

class OfferCreateView(LoginRequiredMixin, OfferFormMixin, CreateView):
    form_class = OfferCreateForm

    def get_context_data(self, **kwargs):
        context = super(OfferCreateView, self).get_context_data(**kwargs)

        context['submit_title'] = _(u'Добавить предложение')
        context['cancel_title'] = _(u'Отмена')
        context['cancel_url'] = reverse('market:offer_list')

        return context

class OfferUpdateView(LoginRequiredMixin, AuthorRequiredMixin, OfferFormMixin, UpdateView):
    form_class = OfferCreateForm

    def dispatch(self, request, *args, **kwargs):
        result = super(OfferUpdateView, self).dispatch(request, *args, **kwargs)
        if self.get_object().offer_type <> 1:
            return HttpResponseForbidden()
        return result

    def get_context_data(self, **kwargs):
        context = super(OfferUpdateView, self).get_context_data(**kwargs)

        context['submit_title'] = _(u'Изменить предложение')
        context['cancel_title'] = _(u'Отмена')
        context['cancel_url'] = reverse('market:detail', kwargs={'pk': self.object.pk})

        return context

class DemandCreateView(LoginRequiredMixin, OfferFormMixin, CreateView):
    form_class = DemandCreateForm

    def get_context_data(self, **kwargs):
        context = super(DemandCreateView, self).get_context_data(**kwargs)

        context['submit_title'] = _(u'Добавить спрос')
        context['cancel_title'] = _(u'Отмена')
        context['cancel_url'] = reverse('market:demand_list')

        return context

class DemandUpdateView(LoginRequiredMixin, AuthorRequiredMixin, OfferFormMixin, UpdateView):
    form_class = DemandCreateForm

    def dispatch(self, request, *args, **kwargs):
        result = super(DemandUpdateView, self).dispatch(request, *args, **kwargs)
        if self.get_object().offer_type <> 0:
            return HttpResponseForbidden()
        return result

    def get_context_data(self, **kwargs):
        context = super(DemandUpdateView, self).get_context_data(**kwargs)

        context['submit_title'] = _(u'Изменить спрос')
        context['cancel_title'] = _(u'Отмена')
        context['cancel_url'] = reverse('market:detail', kwargs={'pk': self.object.pk})

        return context

class OfferDetailView(DetailView):
    model = Offer

    def get_context_data(self, **kwargs):
        context = super(OfferDetailView, self).get_context_data(**kwargs)
        context['gallery'] = Photo.objects.filter(offer=self.object)
        if self.object.offer_type:
            context['cancel_url'] = reverse('market:offer_list')
        else:
            context['cancel_url'] = reverse('market:demand_list')
        # Для того, чтоб включенный шаблон market/_status.html работал и в детальной информации и в глобальном поиске
        context['item'] = self.object
        return context

class BaseListViewMixin(object):
    template_name = 'market/offer_list.html'
    model = Offer
    paginate_by = 10

    def has_changed(self):
        # Т.к. стандартный has_changed формы валится на полях, у которых widget=AutoComboboxSelectWidget
        #  проверяем заполненность формы - вручную
        for key, value in self.form_class.initial.items():
            if self.request.GET.has_key(key) and value <> self.request.GET[key]:
                return True
        return False

    def get_queryset(self):
        queryset = Offer.objects.select_related('location','origin_country').filter(offer_type=self.offer_type).order_by('-date_create')
        try: company_id = self.request.user.employee.company.id
        except: company_id = -1
        partners1 = Partnership.objects.filter(company_partner1=company_id).distinct().values_list('company_partner2__id', flat=True)
        partners2 = Partnership.objects.filter(company_partner2=company_id).distinct().values_list('company_partner1__id', flat=True)
        if self.request.user.is_authenticated():
            extra_select={'is_partner':'exists(select id from deal_partnership where (company_partner1_id = market_offer.company_id and company_partner2_id = %(my_company)s) or (company_partner2_id = market_offer.company_id and company_partner1_id = %(my_company)s))' % {'my_company':company_id}}
        else:
            extra_select={'is_partner':'0'}
        queryset = queryset.extra(select = extra_select)

        offer_view_filter = self.request.GET.get('offer_view_filter')
        if offer_view_filter == 'except_mine':
            queryset = queryset.exclude(user=self.request.user)
        elif offer_view_filter == 'from_partners':
            queryset = queryset.filter(Q(company__in=partners1) | Q(company__in=partners2))
        elif offer_view_filter == 'mine':
            queryset = queryset.filter(user=self.request.user)

        money_type = self.request.GET.get('money_type')
        if money_type:
           queryset = queryset.filter(money_type=money_type)

        changed = self.has_changed()
        if changed:
            self.form = self.form_class(self.request.GET)  # Заполняем форму и применяем проверку полей
        else:
            self.form = self.form_class(initial={'offer_view_filter':offer_view_filter})  # Работаем с пустой формой, чтоб не сообщать об ошибках при переходе по страницам и изменении "мое/партнеров..."



        if changed and self.form.is_valid():
            goods=self.form.cleaned_data.get('goods')
            if goods:
                queryset = queryset.filter(goods=goods)

            location=self.form.cleaned_data.get('location')
            region=self.form.cleaned_data.get('region')
            country=self.form.cleaned_data.get('country')
            if location:
                queryset = queryset.filter(location=location)
            elif region:
                queryset = queryset.filter(region=region)
            elif country:
                queryset = queryset.filter(country=country)

            price_min = self.form.cleaned_data.get('price_min')
            if price_min:
                queryset = queryset.filter(price__gte=price_min)
            price_max = self.form.cleaned_data.get('price_max')
            if price_max:
                queryset = queryset.filter(price__lte=price_max)

            try:
                caliber_min = float(self.form.cleaned_data.get('caliber_min'))
                caliber_max = float(self.form.cleaned_data.get('caliber_max'))
            except:
                caliber_min = caliber_max = None

            if caliber_min and caliber_max:
                queryset = queryset.filter(Q(caliber_min__range=(caliber_min, caliber_max)) | Q(caliber_max__range=(caliber_min, caliber_max)))
            origin_country = self.form.cleaned_data.get('origin_country')
            if origin_country:
                queryset = queryset.filter(origin_country=origin_country)
            payments = self.form.cleaned_data.get('payment_type')
            if payments:
                queryset = queryset.filter(payment_type__in=payments)
            delivery = self.form.cleaned_data.get('delivery')
            if delivery <> None:
                queryset = queryset.filter(delivery=delivery)

        return queryset

    def get_context_data(self, **kwargs):
        context = super(BaseListViewMixin, self).get_context_data(**kwargs)

        context['form'] = self.form
        context['valid'] = 0

        q = kwargs['object_list']
        if q:
            goods = q[0].goods
            q = q.filter(goods=goods, price__gt=0)
            context['mam_pk'] = goods.pk
            context['mam_title'] = goods.title
            context['mam_q'] = q
            context['mam'] = q.aggregate(avg_price=Avg('price'), min_price=Min('price'), max_price=Max('price'))

        if self.has_changed() and self.form.is_valid():
            context.update(self.form.cleaned_data)
            context['payment_type'] = u', '.join(map(lambda x: x.title, context.get('payment_type', [])))
            money_type = self.form.cleaned_data.get('money_type')
            if money_type:
                context['money_type_str'] = money_type.title
            else:
                context['money_type_str'] = ''
            delivery = self.form.cleaned_data.get('delivery')
            if delivery == True: context['delivery'] = _(u'Да')
            elif delivery == False: context['delivery'] = _(u'Нет')
            else: context['delivery'] = _(u'Неважно')
            context['valid'] = 1
        else:
            try: money_type = int(self.request.GET.get('money_type', self.form_class.initial.get('money_type')))
            except: money_type = 0
            context['money_type_str'] = dict(self.form.fields['money_type'].choices).get(money_type, '')
        return context


class OfferListView(BaseListViewMixin, ListView):
    offer_type = 1
    form_class = OfferSearchForm

    def get_context_data(self, **kwargs):
        context = super(OfferListView, self).get_context_data(**kwargs)

        context['submit_title'] = _(u'Найти предложения')
        context['new_url'] = reverse('market:offer_list')

        return context

class DemandListView(BaseListViewMixin, ListView):
    offer_type = 0
    form_class = DemandSearchForm

    def get_context_data(self, **kwargs):
        context = super(DemandListView, self).get_context_data(**kwargs)

        context['submit_title'] = _(u'Найти спрос')
        context['new_url'] = reverse('market:demand_list')

        return context

class OfferDeleteView(LoginRequiredMixin, DeleteView):
    model = Offer

    def delete(self, request, *args, **kwargs):
        id = 0
        offer_type = None
        try: 
            self.object = self.get_object()
            id = self.object.id
            offer_type = self.object.offer_type
            self.object.delete()
        except:
            pass

        return HttpResponse(json.dumps({'id': id, 'offer_type': offer_type}), mimetype="application/json")

class OfferRenewView(LoginRequiredMixin, SingleObjectMixin, View):
    model = Offer

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        self.object.date_create = datetime.datetime.now()
        self.object.save()

        return HttpResponsePermanentRedirect(request.META.get('HTTP_REFERER', '/'))
